//
//  main.m
//  combinepods
//
//  Created by Sebastian Hagedorn on 20.11.12.
//
//


#import <Foundation/Foundation.h>

#pragma mark - Definitions

#define MODULE_KEY @"ModuleName"
#define PODS_KEY @"Pods"
#define MODULE_PLIST_NAME @"ModuleDescription"

#pragma mark - Default values

#define DEFAULT_TARGET_FILE_NAME @"Podfile"

#pragma mark - Script-ish Code

int main(int argc, const char * argv[])
{

    @autoreleasepool {

#pragma mark - Parameters

        // [1] Root directory of the super project (absolute path)
        // [2] Relative path to modules PLIST file

        NSLog(@"argc: %d", argc);
        if (argc < 2) {
            NSLog(@"Mandatory parameters missing. See source code for documentation.");
            return 1;
        }

        NSString *sourceDirPath = [NSString  stringWithUTF8String:argv[1]];
        NSString *activeModulesPlistPath = [[NSString stringWithFormat:@"%@", sourceDirPath] stringByAppendingPathComponent:[NSString stringWithUTF8String:argv[2]]];

        NSLog(@"Mandatory parameters set.");
        NSLog(@"Project Root Directory: %@", sourceDirPath);
        NSLog(@"Modules PLIST: %@", activeModulesPlistPath);

#pragma mark - Read PLIST

        NSArray *activeModulesPlist = [NSArray arrayWithContentsOfFile:activeModulesPlistPath];
        if (!activeModulesPlist) {
            NSLog(@"Modules PLIST could not be found or read: %@", activeModulesPlistPath);
            return 1;
        }

        NSMutableArray *modulePaths = [NSMutableArray array];
        for (NSDictionary *module in activeModulesPlist) {
            NSString *name = [module objectForKey:MODULE_KEY];
            if (name) {
                [modulePaths addObject:name];
            }
        }

#pragma mark - Go into every module's folder and read module PLIST

        NSMutableDictionary *podsCollection = [NSMutableDictionary dictionary];

        for (NSString *name in modulePaths) {
            NSString *path = [[[[sourceDirPath stringByAppendingPathComponent:name] stringByAppendingPathComponent:name] stringByAppendingPathComponent:MODULE_PLIST_NAME] stringByAppendingPathExtension:@"plist"];
            NSDictionary *modulePlist = [NSDictionary dictionaryWithContentsOfFile:path];

            NSArray *pods = [modulePlist objectForKey:PODS_KEY];
            for (NSString *podName in pods) {
                [podsCollection setObject:@(YES) forKey:podName];
                NSLog(@"%@ uses %@", name, podName);
            }
        }

#pragma mark - Create output

        NSMutableString *podfileContent = [NSMutableString stringWithString:@"platform :ios, '6.0'\n\n"];

        for (NSString *podName in podsCollection.allKeys) {
            [podfileContent appendFormat:@"pod '%@'\n", podName];
        }

        NSString *outputPath = [sourceDirPath stringByAppendingPathComponent:DEFAULT_TARGET_FILE_NAME];

        NSLog(@"Write %@ to %@", podfileContent, outputPath);

        NSError *writeError = nil;
        [podfileContent writeToFile:outputPath atomically:YES encoding:NSUTF8StringEncoding error:&writeError];
        if (writeError) {
                NSLog(@"Write Error: %@", writeError);
                return 1;
        }

    NSLog(@"Done.");
    return 0;
    }
}



