# combinepods tool: Short description & copyright/warranty notice #

## INFO ##

This project is not maintained and may include outdated information or code that is
not compatible with recent versions of tools, such as, Xcode and CocoaPods. The
repository is online for reference only.

For current projects, please visit my [GitHub](https://github.com/shagedorn) or
[Stackoverflow](https://stackoverflow.com/users/2050985/hagi) profiles.

combinepods allows you to combine Pod requirements from multiple modules/subprojects
to one global Podfile. It has been developed as part of this project: 
https://bitbucket.org/shagedorn/modular-erp-app

For more information on Pods, see CocoaPods.org.

**Runs on:** Mac OS X 10.7  
**Useful for:** Xcode projects

## LEGAL ##

### COPYRIGHT & WARRANTY NOTICE (combinepods) ###

combinepods was written by Sebastian Hagedorn as part of a study work at
TU Dresden/SALT Solutions GmbH. It was published under the following license (FreeBSD):

> Copyright (c) 2012 Sebastian Hagedorn
> All rights reserved.
> 
> Redistribution and use in source and binary forms, with or without modification,
> are permitted provided that the following conditions are met:
> 
> 1. Redistributions of source code must retain the above copyright notice, this
> list of conditions and the following disclaimer.
> 2. Redistributions in binary form must reproduce the above copyright notice, this
> list of conditions and the following disclaimer in the documentation and/or other
> materials provided with the distribution.
> 
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
> ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
> WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
> IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
> INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
> BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
> DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
> OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
> THE POSSIBILITY OF SUCH DAMAGE.
